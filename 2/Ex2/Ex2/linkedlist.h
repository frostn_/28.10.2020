#ifndef LINKEDLIST_H
#define LINKEDLIST_H

typedef struct Stack {
    int data;
    struct Stack* next;
}Stack;

Stack* newElement(int Data);
void Push(Stack** Head, int Data);
bool isEmpty(Stack* Head);
int Pop(Stack** Head);
#endif // !STACK_H

