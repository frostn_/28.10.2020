// Ex2.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include "stack.h"
using std::cout;
using std::string;

#include <string>

int main()
{
    string _DNA_strand = "GAACAT";//Original DNA strand
	string _complementary_DNA_strand = "";//Complementary DNA strand for the original one
	
	for (int i = 0; i < _DNA_strand.length(); i++)
		if (_DNA_strand[i] != 'A' && _DNA_strand[i] != 'C' && _DNA_strand[i] != 'G' && _DNA_strand[i] != 'T')
			_exit(1);
		else
		{
			if (_DNA_strand[i] == 'A')
				_complementary_DNA_strand += 'T';
			else if (_DNA_strand[i] == 'C')
				_complementary_DNA_strand += 'G';
			else if (_DNA_strand[i] == 'G')
				_complementary_DNA_strand += 'C';
			else
				_complementary_DNA_strand += 'A';
		}

    cout << _complementary_DNA_strand << _DNA_strand << std::endl;
    string RNA_transcript = "";
	
	for (int i = 2; i <= 4; i++)
	{
		if (true)
			if (_complementary_DNA_strand[i] == 'T')
				RNA_transcript += 'U';
			else
				RNA_transcript += _complementary_DNA_strand[i];
		else
			if (_DNA_strand[i] == 'T')
				RNA_transcript += 'U';
			else
				RNA_transcript += _DNA_strand[i];

	}
    cout << RNA_transcript << std::endl;
    RNA_transcript = "Hello";
    //cout << string(RNA_transcript.rbegin(),RNA_transcript.rend());
    //cout << std::count(RNA_transcript.begin(),RNA_transcript.end(),'l');

	_DNA_strand = "AGAACATAACTAAT";
	string toke = "";
	string codon = "AAC";
	unsigned int amount = 0;
	for (size_t offset = _DNA_strand.find(codon); offset != std::string::npos;
	offset = _DNA_strand.find(codon, offset + codon.length()))
	{
		++amount;
	}
	
	RNA_transcript = "AGAACAUAAC";
	cout << "Amount is" << amount;
	cout << RNA_transcript << std::endl;
	RNA_transcript = RNA_transcript.substr(3,RNA_transcript.length());
	cout << RNA_transcript << std::endl;
	list1 = {}
	while (List1 && List2 && List1->data == List2->data) 
    {         
        List1 = List1->next;
        List2 = List2->next;
    }
     
    //Non-empty lists
    if (List1 && List2)
    {
        //CompareStrings characters 
        if (List1->data > List2->data)
        {
            return 1;
        }
        else
        {
            return -1;
        }
    }
    //list2 reaches end before list1
    if(List1 && !List2)
    { 
        return 1;
    }
    //list1 reaches end before list2
    if(List2 && !List1)
    { 
        return -1;
    }
    //If both are same
    //It reaches end. 
    return 0;
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
