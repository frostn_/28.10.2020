// Ex2.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include "linkedlist.h"
using std::cout;


Stack* newElement(int Data)
{
    Stack* newStack = new Stack;
    newStack->data = Data;
    newStack->next = NULL;
    return newStack;
}

void Push(Stack** Head, int Data)
{
    Stack* temp = newElement(Data);
    temp->next = *Head;
    *Head = temp;
    temp = NULL;
    free(temp);
    
    

}


bool isEmpty(Stack* Head)
{
    return Head == NULL;
}

int Pop(Stack** Head)
{   
    if(!isEmpty(*Head))
    {
        int data = (*Head)->data;
        Stack* temp = *Head;
        *Head = (*Head)->next;
        free(temp);
        return data;
    }
    else
    {
        
        return -1;
    }
    

}

/*int main()
{
    Stack* myStack = NULL;
    Push(&myStack, 10);
    Push(&myStack, 20);
    Push(&myStack, 30);
    cout << Pop(&myStack);
    
    cout << Pop(&myStack);
    cout << Pop(&myStack);
    cout << Pop(&myStack);
    

    
}*/

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
