#ifndef STACK_H
#define STACK_H

/* a positive-integer value stack, with no size limit */
typedef struct stack
{
	unsigned int data;
	struct stack* next;
} stack;

void push(stack* s, unsigned int element);
int pop(stack* s); // Return -1 if stack is empty
bool isEmpty(stack* Head);
stack* newElement(int Data);
void initStack(stack* s);
void cleanStack(stack* s);

#endif // STACK_H