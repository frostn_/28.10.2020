// Ex1.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>

#include "queue.h"
using std::cout;

/*typedef struct queue {
    int maxsize;
    int count;
    
    unsigned int* items;
} queue;*/



void initQueue(queue* q, unsigned int size)
{
    

    q->maxsize = size;
    q->count = -1;
    q->items = (unsigned int*)malloc(sizeof(int) * size);

    
}
void cleanQueue(queue* q)
{
    delete(q->items);
    delete(q);
}

int isFull(queue* q)
{
    
    return q->count == q->maxsize -1;	// or return size(pt) == pt->maxsize;
}
void enqueue(queue* q, unsigned int newValue)
{
    if (isFull(q))
    {
        printf("OverFlow\nProgram Terminated\n");
        exit(EXIT_FAILURE);
    }

   
    
    // add an element and increments the top index
    q->items[++q->count] = newValue;
    
}
int isEmpty(queue* q)
{
    return q->count == -1;
}

int dequeue(queue* q)
{
    if (isEmpty(q))
    {
        printf("UnderFlow\nProgram Terminated\n");
        exit(EXIT_FAILURE);
    }

    // decrement stack size by 1 and (optionally) return the popped element
    int res = q->items[0];
    q->count--;
    for (int i = 0; i < q->count+1; ++i)
        q->items[i] = q->items[i + 1];
    

    return res;
}


int main()
{
    queue* Q = new queue;
    initQueue(Q, 5);
    
    enqueue(Q, 1);
    enqueue(Q, 2);
    enqueue(Q, 4);
    enqueue(Q, 3);
    cout << dequeue(Q);
    cout << dequeue(Q);
    cout << dequeue(Q);
    cout << dequeue(Q);
    


    
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
